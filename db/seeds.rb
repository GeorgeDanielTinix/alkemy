require 'faker'


# Creating User
if User.count == 0
  user = User.create!(email: "daniel@tinix.com", password: "123456", password_confirmation: "123456")
end
puts "#{user} was created successfully!"


c1 = Character.create(
  name: Faker::Name.name, 
  age: Faker::Number.number(digits: 2),
  weight: Faker::Number.number(digits: 2),
  story: Faker::Movie.quote  
)
c1.save!

c2 = Character.create(
  name: Faker::Name.name, 
  age: Faker::Number.number(digits: 2),
  weight: Faker::Number.number(digits: 2),
  story: Faker::Movie.quote  
)
c2.save!

g1 =  Gender.create(
  name: Faker::Gender.type,
)
g1.save!

g2 =  Gender.create(
  name: Faker::Gender.type,
)
g2.save!

m1 = Movie.create()
m2 = Movie.create()

m1 = Movie.update(
  title: Faker::Lorem.sentence,
  rating: rand(1..5),
  characters: [c1, c2]
)

m2 = Movie.update(
  title: Faker::Lorem.sentence,
  rating: rand(1..5),
  characters: [c1, c2]
)

c3 = Character.create(
  name: Faker::Name.name, 
  age: Faker::Number.number(digits: 2),
  weight: Faker::Number.number(digits: 2),
  story: Faker::Movie.quote,
  movies: [m1, m2]
)
c3.save!
 

g1 =  Gender.update(
  name: Faker::Gender.type,
  movies: [m1, m2]
)

g2 =  Gender.update(
  name: Faker::Gender.type,
  movies: [m1, m2]
)


