class CreateMovies < ActiveRecord::Migration[6.0]
  def change
    create_table :movies do |t|
      t.string :title
      t.integer :rating
      t.belongs_to :gender, index: true

      t.timestamps
    end
  end
end
