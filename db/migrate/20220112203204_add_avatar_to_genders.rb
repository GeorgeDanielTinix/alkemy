class AddAvatarToGenders < ActiveRecord::Migration[6.0]
  def change
    add_column :genders, :avatar, :string
  end
end
