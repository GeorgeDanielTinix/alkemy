class AddAvatarToMovies < ActiveRecord::Migration[6.0]
  def change
    add_column :movies, :avatar, :string
  end
end
