class Gender < ApplicationRecord
 mount_uploader :avatar, AvatarUploader
 serialize :avatars, JSON

  # Associations
  has_many :movies, dependent: :destroy
  
  # Validations
  validates :name, presence: true
  validates :name, length: {in: 1..30 }
  
  def as_json(options={})
    h = super(options)
    h
  end
end
