class Movie < ApplicationRecord
  mount_uploader :avatar, AvatarUploader
  serialize :avatars, JSON

  # Associations
  has_many :subjects
  has_many :characters, through: :subjects, dependent: :destroy
  belongs_to :gender

  # Scopes
  scope :filter_by_title, ->(keyword) { where('title LIKE ?', "%#{keyword}%") }
  scope :by_created_desc, ->(desc) { order(created_at: :desc) }
  scope :by_created_asc, ->(asc) { order(created_at: :asc) }
  scope :with_references_gender, ->(gender_id) { joins(:gender).where('gender_id = ?', gender_id) }

  # Validations
  validates :title, presence: true, length: { in: 5..50 }
  validates :rating, presence: true
  validates :rating, numericality: { less_than_or_equal_to: 5, only_integer: true}

  def as_json(options = {})
    super(options)
  end

  # Search query parameters
  def self.search(params = {})
      movies = params[:movies_ids].present? ? Movies.find(params[:movie_ids]) : Movie.all
      
      if params[:title].instance_of?(String)
        movies = movies.filter_by_title(params[:title]) if params[:title] == params[:title]
      elsif params[:order] == 'desc'
        movies = movies.by_created_desc(params[:order]) if params[:order] == params[:order]
      elsif params[:order] == 'asc'
        movies = movies.by_created_asc(params[:order]) if params[:order] == params[:order]
      elsif params[:gender].to_i.instance_of?(Integer)
        movies = movies.with_references_gender(params[:gender].to_i) if params[:gender] == params[:gender]
      else
         "unknown parameter"
      end
    movies
  end
end
