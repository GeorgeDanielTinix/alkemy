class Character < ApplicationRecord
  mount_uploader :avatar, AvatarUploader
  serialize :avatars, JSON

  # Associations
  has_many :subjects
  has_many :movies, through: :subjects, dependent: :destroy

  # Scopes
  scope :filter_by_name, ->(keyword) { where('name LIKE ?', "%#{keyword}%") }
  scope :equal_to_age, ->(age) { where('age = ?', age) }

  scope :with_references_movies, ->(movie_id) { joins(:movies).where('movie_id = ?', movie_id) }
  
  # Validations
  validates :name, presence: true, length: { in: 5..30 }
  validates :age, presence: true
  validates :age, numericality: { less_than_or_equal_to: 100, only_integer: true}
  validates :story, presence: true, length: { maximum: 200 }
  validates :weight, presence: true
  validates :weight, numericality: { less_than_or_equal_to: 100, only_integer: true}

  def as_json(options = {})
    super(options)
  end

  # Search query parameters
  def self.search(params = {})
      characters = params[:character_ids].present? ? Character.find(params[:character_ids]) : Character.all

      if params[:name].instance_of?(String)
        characters = characters.filter_by_name(params[:name].to_s) if params[:name] == params[:name]
      elsif params[:age].to_i.instance_of?(Integer)
        characters = characters.equal_to_age(params[:age].to_i) if params[:age] == params[:age]
      elsif params[:movies].to_i.instance_of?(Integer)
        characters = characters.with_references_movies(params[:movies].to_i) if params[:movies] == params[:movies]
      else
        'unknown parameter'
      end
    characters
  end
end
