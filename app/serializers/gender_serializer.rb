class GenderSerializer < ActiveModel::Serializer
  attributes :id, :name, :avatar
  has_many :movies
end
