class MovieSerializer < ActiveModel::Serializer
  # Movie should show image title and created_at
  attributes :title, :avatar, :created_at


  has_many :characters, through: :subjects 
  belongs_to :gender
end
