class CharacterSerializer < ActiveModel::Serializer
  attributes :id, :name, :age, :weight, :story, :avatar

  has_many :subjects 
  has_many :movies, through: :subjects 
end
