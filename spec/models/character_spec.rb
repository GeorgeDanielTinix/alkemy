require 'rails_helper'

RSpec.describe Character, type: :model do
  subject { Character.new(name: "Robin Williams", age: 25, weight: 70, story: "test for a long story") }

  it "name should be present" do
    subject.name = nil
    expect(subject).to_not be_valid 
  end

  it "the name should not be short" do
    subject.name = "n"
    expect(subject).to_not be_valid
  end

  it "the name should equal to 5" do
    subject.name = "n" * 5
    expect(subject).to be_valid
  end

  it "the name should greater than 5" do
    subject.name = "n" * 30
    expect(subject).to be_valid
  end

  it "age should be present" do
    subject.age = nil
    expect(subject).to_not be_valid 
  end
  
  it "age should be equal to 1" do
    subject.age = 1
    expect(subject).to be_valid 
  end

  it "age should be equal to 99" do
    subject.age = 99
    expect(subject).to be_valid
  end

  it "age should not be greater than 99" do
    subject.age = 110
    expect(subject).to_not be_valid
  end

  it "the story should be present" do
    subject.story = nil
    expect(subject).to_not be_valid
  end
  
  it "the story should be length maximum 200" do
    subject.story = "s" * 200
    expect(subject).to be_valid
  end

  it "the story should not be length that 200" do
    subject.story = "s" * 250
    expect(subject).to_not be_valid
  end

  it "the weight should be present" do
    subject.weight = nil
    expect(subject).to_not be_valid 
  end
  
  it "the weight should be length maximum 99" do
    subject.weight = 100
    expect(subject).to be_valid
  end

  it "the weight should not be length that 200" do
    subject.weight = 200
    expect(subject).to_not be_valid
  end
end
