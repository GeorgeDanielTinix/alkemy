require "rails_helper"

RSpec.describe User, type: :model do
  subject { User.new(email: "john@doe.com", name:"John Doe") }

  before { subject.save }

  it "name should be present" do
    subject.name = nil
    expect(subject).to_not be_valid
  end
  
  it "email should be present" do
    subject.email = nil
    expect(subject).to_not be_valid
  end

  it "email should not be to sort" do
    subject.email ="a"
    expect(subject).to_not be_valid
  end
  
  it "email should be length maximum 100 " do
    subject.email = "a" * 100
    expect(subject).to_not be_valid
  end

  it "email should be valid format to regex" do
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
		subject.email = VALID_EMAIL_REGEX
    expect(subject).to_not be_valid
  end
end
