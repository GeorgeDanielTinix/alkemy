require 'rails_helper'

RSpec.describe Gender, type: :model do
  subject { Gender.new(name: "test to gender") }

  before { subject.save }

  it "gender should be present" do
    subject.name = nil
    expect(subject).to_not be_valid
  end

  it "gender should not be greater to 30" do
    subject.name = "g" * 31
    expect(subject).to_not be_valid
  end
  
  it "gender should be valid between 1 to 30" do
    subject.name = "g" * 25
    expect(subject).to be_valid
  end
end
