require 'rails_helper'

RSpec.describe Movie, type: :model do
  subject { Movie.new(title: "this is a test", rating: 5 ) }

  before { subject.save }

  it " the title should be present" do
    subject.title = nil
    expect(subject).to_not be_valid
  end
 
  it "the title should not be short" do
    subject.title = "t"
    expect(subject).to_not be_valid
  end

  it "title should be greater or equal to five" do
    subject.title = "t" * 51
    expect(subject).to_not be_valid
  end

  it "rating should be present" do
    subject.rating = nil
    expect(subject).to_not be_valid
  end

  it "rating should not maximum to five" do
    subject.rating = 6
    expect(subject).to_not be_valid
  end

  it "movie has a gender association" do
    movie = create(:movie)
    expect(movie.gender).to be_kind_of(Gender)
  end
end
