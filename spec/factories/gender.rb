FactoryBot.define do
  factory :gender do
    name { Faker::Name.name }
  end
end
