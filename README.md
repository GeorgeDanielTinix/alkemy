# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version 2.7.0

* Rails version 6.0.4.4

* Bundler version 2.1.2

* $rake db:create & rake db:migrate
* $rake db:seed

* to bring up the server
* $foreman start

# API documentation

### POST /api/auth
Registration user

```json
{
  "email": "daniel@tinix.dev",
  "password": "test123",
  "password_confirmation": "test123"
}
```

### POST /api/auth/sign_in
Login user a new session.

Params: Json raw:

```json
{
  "email": "daniel@tinix.dev",
  "password": "test123"
}
```


### DELETE /api/auth/sign_out
Authorization method: Bearer token 
(correct JWT[access-token, client, uid] for a specific user)

Logout user


```json
{
  "email": "daniel@tinix.com",
  "password": "123456",
  "password_confirmation": "123456"
}
```

## GET /api/v1/movies
Authorization method: Bearer token,
(correct JWT[access-token, client, uid] for a specific user)

Return all movies 

## GET /api/v1/movies?title=title
Authorization method: Bearer token,
(correct JWT[access-token, client, uid] for a specific user)

return only the movie with that title

## GET /api/v1/movies?gender=idGender
Authorization method: Bearer token,
(correct JWT[access-token, client, uid] for a specific user)

return genders with idGender asociation with the movie

## GET /api/v1/movies?order=asc
Authorization method: Bearer token,
(correct JWT[access-token, client, uid] for a specific user)

return all movies with ascendants order


## GET /api/v1/movies?order=desc
Authorization method: Bearer token,
(correct JWT[access-token, client, uid] for a specific user)

return all movies with descending order


## GET /api/v1/movies/id
Authorization method: Bearer token,
(correct JWT[access-token, client, uid] for a specific user)

Return show details of specific movie id

## DELETE /api/v1/movies/id 
Authorization method: Bearer token,
(correct JWT[access-token, client, uid] for a specific user)

 Only delete the movie by id

## PUT/PATH /api/v1/movies/id
Authorization method: Bearer token,
(correct JWT[access-token, client, uid] for a specific user)

Params: JSON raw, body example:

```json
{
  "title": "my first test title",
  "avatar": {
    "url": "/uploads/character/avatar.png"
	}
}

```

## POST /api/v1/movies
Authorization method: Bearer token,
(correct JWT[access-token, client, uid] for a specific user)

Params: JSON raw, body example:

```json
{
  "title": "create last movie",
  "rating": 5,
  "gender_id": 1
}
```


## GET /api/v1/character
Authorization method: Bearer token,
(correct JWT[access-token, client, uid] for a specific user)

Return all characters

## GET /api/v1/characters?name=name
Authorization method: Bearer token,
(correct JWT[access-token, client, uid] for a specific user)

return only the character with exactly the name


## GET /api/v1/characters?age=edad
Authorization method: Bearer token,
(correct JWT[access-token, client, uid] for a specific user)

return only the character with exactly the age

## GET /api/v1/characters?movies=idMovie
Authorization method: Bearer token,
(correct JWT[access-token, client, uid] for a specific user)

return only the character with exactly the associated movieID
 
## GET /api/v1/characters/id 
Authorization method: Bearer token,
(correct JWT[access-token, client, uid] for a specific user)

Return show details of specific character id


## DELETE /api/v1/characters/id 
Authorization method: Bearer token,
(correct JWT[access-token, client, uid] for a specific user)

Only delete the character by id



## PUT/PATH /api/v1/characters/id 
Authorization method: Bearer token,
(correct JWT[access-token, client, uid] for a specific user)

Params: JSON raw, body example:

```json
{
  "name": "Daniel Tinivella.",
  "age": 45,
  "weight": 90,
  "story": "All right"
}
```

Update character by id and return updated successfully



## POST /api/v1/characters
Authorization method: Bearer token,
(correct JWT[access-token, client, uid] for a specific user)

Params: JSON raw, body example:

```json
{ 
  "name": "my personal character",
  "age": 25,
  "weight": 92,
  "story": "is a long story"
}
```


